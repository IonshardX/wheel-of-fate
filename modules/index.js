/* global $ WebSocket */
import { RandomSpinner } from './spinner.js'
import { Base64 } from 'https://cdn.jsdelivr.net/npm/js-base64@3.5.2/base64.mjs'
import { v4 as uuidv4 } from 'https://jspm.dev/uuid'

const serverUrl = 'wss://ml53ly5cpj.execute-api.us-east-2.amazonaws.com/dev'
const defaultOptions = [
  {
    label: 'Milk',
    color: '#969696',
    chance: 0.02
  },
  {
    label: 'Cranberry Juice',
    color: '#ff3b3b',
    chance: 0.05
  },
  {
    label: 'Peach Juice',
    color: '#ffe5b4',
    chance: 0.15
  },
  {
    label: 'Orange Juice',
    color: '#ff7b03',
    chance: 0.20
  },
  {
    label: 'Diet Coke',
    color: '#5632dc',
    chance: 0.20
  },
  {
    label: 'Water',
    color: '#209cee'
  }
]

const canvas = document.getElementById('canvas')
const sync = { connected: false }
let spinner
let options

function copyInput (id) {
  const elem = document.getElementById(id)
  elem.select()
  elem.setSelectionRange(0, 9999999)
  document.execCommand('copy')
}

function encodeData (data) {
  return Base64.encodeURI(JSON.stringify(options))
}

function decodeData (string) {
  try {
    return JSON.parse(Base64.decode(string))
  } catch {
    return null
  }
}

function addSegment () {
  options[options.length - 1].chance = 0.01
  options.push({ label: '', color: '#ccc' })
}

function removeSegment (index) {
  options.splice(index, 1)
  options[options.length - 1].chance = null
}

function moveSegment (index, direction) {
  const temp = options[index]
  options[index] = options[index + direction]
  options[index + direction] = temp

  if (options[index].chance === null) {
    options[index].chance = 0.01
  }
  if (options[index + direction].chance === null) {
    options[index + direction].chance = 0.01
  }
  options[options.length - 1].chance = null
}

function createSegmentEntry (index, { label, color, chance }) {
  const segmentHTML = `
    <div class="field is-horizontal segment-entry" data-index="${index}">
        <div class="field-body">
            <div class="field color-field is-narrow">
                <div class="control">
                    <input class="segment-color" value="${color}" data-index="${index}">
                </div>
            </div>

            <div class="field is-expanded">
                <div class="control">
                    <input class="input segment-label" type="text" placeholder="Label" value="${label}" data-index="${index}">
                </div>
            </div>

            <div class="field has-addons is-narrow">
                <div class="control">
                    <input class="input segment-chance" type="number" placeholder="Chance" min="1" max="99" value="${Math.round(chance * 100)}" data-index="${index}">
                </div>
                <div class="control">
                    <a class="button is-static">
                        %
                    </a>
                </div>
            </div>
            
            <div class="field is-grouped is-narrow">
              <div class="control">
                <button class="button move-segment-up" data-index="${index}">
                  <i class="fas fa-arrow-up"></i>
                </button>
              </div>
              <div class="control">
                <button class="button move-segment-down" data-index="${index}">
                  <i class="fas fa-arrow-down"></i>
                </button>
              </div>
              <div class="control">
                <button class="button delete-segment is-danger" data-index="${index}">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </div>
            </div>
        </div>
    </div>
  `
  return $.parseHTML(segmentHTML)
}

function remainingChance (options) {
  const chances = options.slice(0, options.length - 1).map(x => x.chance)
  const sum = chances.reduce((x, y) => x + y, 0)
  const remainder = 1 - sum
  return remainder
}

function syncOptions () {
  if (sync.paused) {
    sync.paused = false
    return
  }

  if (sync.connected) {
    sync.socket.send(JSON.stringify({
      action: 'sync',
      options: options
    }))
  }
}

function update () {
  syncOptions()

  const segmentList = $('#segment-list')

  segmentList.empty()

  options.forEach((option, index) => {
    segmentList.append(createSegmentEntry(index, option))
  })

  segmentList.find('.segment-chance').last().prop('disabled', true)
  segmentList.find('.segment-chance').last().val(Math.round(remainingChance(options) * 100))

  segmentList.find('.segment-color').spectrum({
    type: 'color',
    showPalette: false,
    showInput: 'true',
    showAlpha: 'false',
    allowEmpty: 'false',
    replacerClassName: 'color-input',
    change: function (color) {
      const index = $(this).data('index')
      options[index].color = color.toHexString()
      update()
    }
  })

  segmentList.find('.segment-label').change(function () {
    const index = $(this).data('index')
    const label = $(this).val()
    options[index].label = label
    update()
  })

  segmentList.find('.segment-chance').change(function () {
    const index = $(this).data('index')
    const chance = $(this).val()
    options[index].chance = chance / 100
    update()
  })

  if (options.length === 1) {
    segmentList.find('.delete-segment').prop('disabled', true)
  }

  segmentList.find('.delete-segment').click(function () {
    const index = $(this).data('index')
    removeSegment(index)
    update()
  })

  segmentList.find('.move-segment-up').first().prop('disabled', true)

  segmentList.find('.move-segment-up').click(function () {
    const index = $(this).data('index')
    moveSegment(index, -1)
    update()
  })

  segmentList.find('.move-segment-down').last().prop('disabled', true)

  segmentList.find('.move-segment-down').click(function () {
    const index = $(this).data('index')
    moveSegment(index, 1)
    update()
  })

  spinner = RandomSpinner(canvas, options)
}

function pingServer () {
  if (!sync.connected) {
    return
  }

  sync.socket.send(JSON.stringify({
    action: 'ping'
  }))

  setTimeout(pingServer, 20000)
}

function connectSync (wheelId) {
  sync.wheelId = wheelId
  sync.socket = new WebSocket(`${serverUrl}?wheel=${sync.wheelId}`)
  sync.socket.onopen = function (e) {
    const url = new URL(window.location.pathname, window.location.origin)
    url.searchParams.set('wheel', sync.wheelId)
    $('#sync-link').val(url)
    $('#sync-copy').removeClass('is-static')
    sync.connected = true
    setTimeout(pingServer, 20000)
  }
  sync.socket.onmessage = function (e) {
    const data = JSON.parse(e.data)
    switch (data.action) {
      case 'spin':
        spinWheel(data.data)
        break
      case 'sync':
        options = data.options
        sync.paused = true
        update()
        break
      case 'requestSync':
        syncOptions()
        break
      default:
        break
    }
  }
  sync.socket.close = function (e) {
    disconnectSync()
  }
  sync.socket.error = function (e) {
    disconnectSync()
  }

  $('#stop-sync').css('display', 'block')
  $('#start-sync').css('display', 'none')
}

function disconnectSync () {
  sync.connected = false
  $('#stop-sync').css('display', 'none')
  $('#start-sync').css('display', 'block')
  $('#sync-link').val('')
  $('#sync-copy').addClass('is-static')
}

function spinWheel (data) {
  const resultDiv = $('#result')
  resultDiv.text('Spin in progress!')
  resultDiv.addClass('is-light')
  spinner.spin(data).then((result) => {
    resultDiv.removeClass('is-light')
    resultDiv.text('Result: ' + result.label)
  })
}

function rand (min, max) {
  return Math.random() * (max - min) + min
}

function remoteSpin () {
  sync.socket.send(JSON.stringify({
    action: 'spin',
    data: {
      r: Math.random(),
      speed: rand(10.0, 20.5),
      slowDown: rand(0.03, 0.10)
    }
  }))
}

function initialize () {
  options = decodeData(window.location.hash) || defaultOptions
  window.location.hash = ''

  const newSize = Math.min(canvas.parentNode.parentElement.clientWidth, 500) - 30
  canvas.width = newSize
  canvas.height = newSize
  canvas.style.width = newSize
  canvas.style.height = newSize

  const wheelId = new URLSearchParams(window.location.search).get('wheel')
  if (wheelId) {
    connectSync(wheelId)
  }

  update()

  $('#btnAddSegment').click(function () {
    addSegment()
    update()
  })

  $('#spin').mousedown(function () {
    if (sync.connected) {
      remoteSpin()
    } else {
      spinWheel()
    }
  })

  $('#start-sync').click(function () {
    connectSync(uuidv4())
  })

  $('#stop-sync').click(function () {
    disconnectSync()
  })

  $('#sync-button').click(function () {
    $('#sync-modal').addClass('is-active')
  })

  $('#sync-copy').click(function () {
    copyInput('sync-link')
  })

  $('.close-sync').click(function () {
    $('#sync-modal').removeClass('is-active')
  })

  $('#share-button').click(function () {
    $('#share-modal').addClass('is-active')
    const url = new URL(window.location.pathname, window.location.origin)
    url.hash = encodeData(options)
    $('#share-link').val(url)
  })

  $('#share-copy').click(function () {
    copyInput('share-link')
  })

  $('.close-share').click(function () {
    $('#share-modal').removeClass('is-active')
  })
}

initialize()
