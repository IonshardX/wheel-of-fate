
function Spinner (canvas, options) {
  const ctx = canvas.getContext('2d')
  const width = canvas.width
  const center = width / 2

  function deg2rad (deg) {
    return deg * Math.PI / 180
  }

  function drawSlice (rotation, sliceDeg, color) {
    ctx.beginPath()
    ctx.fillStyle = color
    ctx.moveTo(center, center)
    ctx.arc(center, center, width / 2, deg2rad(rotation), deg2rad(rotation + sliceDeg))
    ctx.lineTo(center, center)
    ctx.fill()
  }

  function drawText (rotation, text) {
    const textSize = Math.round(width / 2 * 0.9)
    ctx.save()
    ctx.translate(center, center)
    ctx.rotate(deg2rad(rotation))
    ctx.textAlign = 'right'
    ctx.fillStyle = '#fff'
    ctx.font = 'bold 24px sans-serif'
    ctx.fillText(text, textSize, 10)
    ctx.restore()
  }

  function drawSegment (label, color, rotation, sliceDeg) {
    // console.log('!!! drawSegment', label, rotation, sliceDeg)
    drawSlice(rotation, sliceDeg, color)
    drawText(rotation + sliceDeg / 2, label)
  }

  function render (startRotation) {
    // console.log('!!! drawSpinner', startRotation)
    let rotation = -90 + startRotation
    const chances = options.slice(0, options.length - 1).map(x => x.chance)
    const sum = chances.reduce((x, y) => x + y, 0)
    const remainder = 1 - sum
    // console.log('!!! Chances:', chances, sum, remainder);
    chances.push(remainder)
    ctx.clearRect(0, 0, width, width)
    for (let i = 0; i < options.length; i++) {
      const option = options[i]
      const sliceDeg = 360 * chances[i]
      drawSegment(option.label, option.color, rotation, sliceDeg)
      rotation += sliceDeg
    }
  }

  return { render }
}

function AnimatedSpinner (canvas, options) {
  const spinner = Spinner(canvas, options)
  const stopSpeed = 5.0
  let resultAngle
  let rotation
  let speed
  let slowDown
  let stopping
  let raf
  let resolveSpin

  function rand (min, max) {
    return Math.random() * (max - min) + min
  }

  function finished () {
    return stopping &&
      rotation < resultAngle &&
      (rotation + speed) > resultAngle
  }

  function distanceToResult () {
    if (rotation > resultAngle) {
      // If we've passed the circle we have to go around again
      return ((360 - rotation) + resultAngle)
    } else {
      return (resultAngle - rotation)
    }
  }

  function animate () {
    if (finished()) {
      stop()
      return
    } else if (stopping) {
      const minSpeed = 2.0
      const distance = distanceToResult()
      const progress = 1.0 - (distance / 360)
      const slowSpeed = stopSpeed + (progress * (minSpeed - stopSpeed))
      // console.log('!!! Stopping - Goal:', resultAngle, 'Current:', rotation, 'Progress:', progress, 'Speed:', slowSpeed)
      rotation += slowSpeed
      rotation %= 360
    } else {
      rotation += speed
      rotation %= 360
      speed -= slowDown

      if (speed < stopSpeed) {
        stopping = true
      }
    }

    spinner.render(rotation)
    raf = window.requestAnimationFrame(animate)
  }

  function spin (result, data = {}) {
    resultAngle = result

    speed = data.speed || rand(10.0, 20.5)
    slowDown = data.slowDown || rand(0.03, 0.10)
    rotation = 0
    stopping = false
    animate()
    return new Promise(resolve => { resolveSpin = resolve })
  }

  function stop () {
    rotation = resultAngle
    speed = 0
    window.cancelAnimationFrame(raf)
    spinner.render(rotation)
    resolveSpin()
  }

  spinner.render(0)

  return { spin }
}

function RandomSpinner (canvas, options) {
  const spinner = AnimatedSpinner(canvas, options)
  var r

  function pickOption (r, options) {
    let sum = 0
    for (let i = 0; i < options.length; i++) {
      sum += options[i].chance
      if (sum > r) {
        return options[i]
      }
    }
    return options[options.length - 1]
  }

  function spin (data = {}) {
    r = data.r || Math.random()
    const resultAngle = 360 - (360 * r)
    console.log('!!! Spinner', r)
    return spinner.spin(resultAngle, data)
      .then(() => pickOption(r, options))
  }

  return { spin }
}

export { Spinner, AnimatedSpinner, RandomSpinner }
